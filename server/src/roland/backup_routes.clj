(ns roland.routes.services
  (:require [ring.util.http-response :refer :all]
            [roland.db.core :as db]
            [compojure.api.sweet :refer :all]
            [schema.core :as s]))


(def not-found-body {:message "not-found"})

(defn now []
  (java.time.LocalDate/now))

(defn parse-interval [str]
  (let [[_ amount unit] (re-matches #"^(\d+)([dwmy])$" str)]
    (when (and amount unit) ;; successful match
      (cond
        (= unit "d") (fn [t] (.plusDays t (Long/valueOf amount)))
        (= unit "w") (fn [t] (.plusWeeks t (Long/valueOf amount)))
        (= unit "m") (fn [t] (.plusMonths t (Long/valueOf amount)))
        (= unit "y") (fn [t] (.plusYears t (Long/valueOf amount)))))))


(defn view-link [l] (db/view-link! {:id (get l :id) :display_on ((parse-interval (get l :display_interval)) (get l :display_on))}))

(s/defschema Link {:id Long
                   :title String
                   :url String
                   :display_interval String
                   (s/optional-key :created_at) s/Any
                   (s/optional-key :display_on) s/Any})

(s/defschema NewLink {:title String
                   :url String
                   :display_interval String})

(s/defschema Links [Link])

(defapi service-routes
  {:swagger {:ui "/swagger-ui"
             :spec "/swagger.json"
             :data {:info {:version "0.0.1"
                           :title "Roland Links API"
                           :description "Link Service based on serving links at intervals"}}}}



  (context "/api" []
    :tags ["links"]


    (GET "/links" []
         :return       Links
         :summary      "Return all links"
         (ok (db/get-links)))

    (GET "/link" []
         :return       Link
         :query-params [id :- Long]
         :responses {200 {:schema Link}
                     404 {:schema s/Any}}
         :summary      "Return link by id"
         (let [db-result (db/get-link {:id id})]
           (if db-result
             (do (ok db-result))
             (do (not-found not-found-body)))))

    (POST "/link/update" []
          :return       s/Bool
          :body [newlink Link]
          :summary      "Updates a link with a given id"
          (let [existing-link (db/get-link {:id (get newlink :id)})]
            (if (= (get newlink :id) (get existing-link :id))
              (do (ok (let [inserted (db/update-link! newlink)] (cond
                                                                  (= inserted 0) false
                                                                  (= inserted 1) true
                                                                  :else false))))
              (do (not-found not-found-body)))))

    (DELETE "/link" []
            :return       s/Bool
            :query-params [id :- Long]
            :summary      "Delete a link given an id"
            (let [existing-link (db/get-link {:id id})]
              (if (= id (get existing-link :id))
                (do (ok (let [deleted (db/delete-link! {:id id})] (cond
                                                                    (= deleted 0) false
                                                                    (= deleted 1) true
                                                                    :else false))))
                (do (not-found not-found-body)))))

    (POST "/link" []
          :return       s/Bool
          :body [newlink NewLink]
          :summary      "Adds a new link to the db"
          (ok (let [inserted (db/create-link!
                              (assoc newlink
                                     :created_at (now)
                                     :display_on ((parse-interval (get newlink :display_interval)) (now))))]
                (cond
                  (= inserted 0) false
                  (= inserted 1) true
                  :else false))))

    (GET "/links/today" []
         :return       Links
         :summary      "Return links that have display_on today's date"
         (ok (db/today-links {:display_on (java.time.LocalDate/now)})))

    (GET "/link/view" []
         :return       s/Bool
         :query-params [id :- Long]
         :summary      "Mark a link as viewd and write it's next view date"
         (let [existing-link (db/get-link {:id id})]
           (if existing-link
             (do (ok (let [updated (view-link existing-link)] (cond
                                                                (= updated 0) false
                                                                (= updated 1) true
                                                                :else false))))
             (do (not-found not-found-body)))))))

