(ns roland.core
  (:require [liberator.core :refer [resource defresource]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.json :refer [wrap-json-body]]
            [roland.db :as db]
            [roland.auth :as auth]
            [java-time :as time]
            ;; [roland.helpers :as aux]
            [buddy.sign.jwt :as jwt]
            [compojure.core :refer [defroutes ANY]]))

(def sampleLink {:id 1 :title "Test_title" :url "https://www.test.com" :display_interval "1m"})
;; (def sampleLinks (vector sampleLink  sampleLink sampleLink))
(def sampleLinks (into [] (repeat 10 sampleLink)))

(defn parse-int [s]
  (Integer/parseInt (re-find #"\A-?\d+" s)))

(defn convert-keys [my-map]
  (into {}
        (for [[k v] my-map]
          [(keyword k) v])))

(defroutes app
  ;; (ANY "/foo" [] (resource :available-media-types ["text/html"]
  ;;                          :handle-ok "<html>Hello, World.</html>"))

  ;; get info on a user
  (ANY "/user" [] (resource :available-media-types ["application/json"]
                            ;; :allowed-methods [:post :get :put]
                            :authorized? (fn [ctx] (auth/is-lvl "read" ctx))
                            :handle-ok (fn [ctx] sampleLinks)))

  ;; add and delete links
  ;; curl -i -XPOST -H 'Content-Type: application/json' -H 'Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyaWQiOiI1YmE2MGJkNzljM2FjNjFlMjU1ZGIxM2YiLCJsdmwiOiJ3cml0ZSIsInVzZXJuYW1lIjoiamFjayJ9.RTJEZzf7hHzhDZTet7z2nT7Pe-NcgWYHNVyOegTAoag' -d '{"url":"http:www.test3.com", "title":"Title"}'  'localhost:3000/protected/link'
  ;; curl -i -XDELETE -H 'Content-Type: application/json' -H 'Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyaWQiOiI1YmE2MGJkNzljM2FjNjFlMjU1ZGIxM2YiLCJsdmwiOiJ3cml0ZSIsInVzZXJuYW1lIjoiamFjayJ9.RTJEZzf7hHzhDZTet7z2nT7Pe-NcgWYHNVyOegTAoag' -d '{"url":"http://www.second.goodlink", "title":"test title 2", "interval":"1d"}'  'localhost:3000/protected/link'
  (ANY "/protected/link" [] (resource :available-media-types ["application/json"]
                            ;; :put
                                      :allowed-methods [:post :delete]
                                      :authorized? (fn [ctx] (auth/is-lvl "write" ctx))
                                      :delete! (fn [ctx] (let [x (get-in ctx [:request :body])]
                                                           (db/delete-link (auth/get-user-id ctx) (convert-keys x))))
                                      :malformed? (fn [ctx] (let [x (get-in ctx [:request :body])]
                                                              (not (db/validate-half-link (convert-keys x)))))
                                      :post! (fn [ctx] (let [x (get-in ctx [:request :body])]
                                                         (db/insert-link (auth/get-user-id ctx) (convert-keys x))))))

  ;; get all users links
  ;; curl -X GET --header 'Accept: application/json' --header 'Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyaWQiOiI1YmE2MGJkNzljM2FjNjFlMjU1ZGIxM2YiLCJsdmwiOiJ3cml0ZSIsInVzZXJuYW1lIjoiamFjayJ9.RTJEZzf7hHzhDZTet7z2nT7Pe-NcgWYHNVyOegTAoag' 'localhost:3000/link'
  (ANY "/link" [] (resource :available-media-types ["application/json"]
                            :allowed-methods [:get]
                            :authorized? (fn [ctx] (auth/is-lvl "read" ctx))
                            :handle-ok (fn [ctx] (db/get-links (auth/get-user-id ctx)))))

;; (ANY "/link/:id" [id] (fn [req] (resource
  ;;                                  :exists? (fn [ctx] (if (= (parse-int id) (:id sampleLink)) {:entity sampleLink}))
  ;;                                  :handle-ok :entity))))
  (ANY "/today/link" [] (resource :available-media-types ["application/json"]
                                  :allowed-methods [:get]
                                  :authorized? (fn [ctx] (auth/is-lvl "read" ctx))
                                  :handle-ok (fn [ctx] (db/get-today-links (auth/get-user-id ctx)))))

  (ANY "/backlog/link" [] (resource :available-media-types ["application/json"]
                                :allowed-methods [:get]
                                :authorized? (fn [ctx] (auth/is-lvl "read" ctx))
                                :handle-ok (fn [ctx] (db/get-backlog-links (auth/get-user-id ctx))))))

(def handler
  (-> app
      wrap-params
      wrap-json-body))

;; https://enyert.github.io/rest-api-clojure-tutorial/
;; http://clojure-liberator.github.io/liberator/doc/resource-definition.html
;; http://funcool.github.io/buddy-sign/latest/#jwt
;; http://clojuremongodb.info/articles/getting_started.html

;; (use '[ring.middleware.json :only [wrap-json-body]]
;;      '[ring.util.response :only [response]])

;; (defn handler [request]
;;   (prn (get-in request [:body "user"]))
;;   (response "Uploaded user."))

;; (def app
;;   (wrap-json-body handler {:keywords? true :bigdecimals? true}))

;; (prn x)))))
