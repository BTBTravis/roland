(ns roland.auth
  (:require [buddy.sign.jwt :as jwt]
            [roland.db :as db]))

(def jwt-key
  (or (System/getenv "ROLAND_JWT_KEY")
      "xxxxrandomxxxx"))

(defn create-user [username]
  ;; add user to db
  (let [newUserId (db/get-user-id (db/insert-user {:username username}))]
    ;; create API JWT tokens
    (when newUserId
      {:write (jwt/sign {:userid newUserId :lvl "write" :username username} jwt-key)
       :read (jwt/sign {:userid newUserId :lvl "read" :username username} jwt-key)})))

;; string --> hash|false
(defn safe-unsign [jwt-token]
  (try
    (jwt/unsign jwt-token jwt-key)
    (catch Exception e nil)))

;; string --> string
(defn get-user-lvl [jwt-token]
  (:lvl (safe-unsign jwt-token)))


;; string --> bool
(defn can-user-read [jwt-token]
  (or  (= "read" (get-user-lvl jwt-token))
       (= "write" (get-user-lvl jwt-token))))

;; string --> bool
(defn can-user-write [jwt-token]
  (= "write" (get-user-lvl jwt-token)))

;; string --> string
(defn extract-user-id [jwt-token]
  (:userid (safe-unsign jwt-token)))


;; string ring-reqponce --> bool
(defn get-user-id [ctx]
  (let [jwt-token (get-in ctx [:request :headers "authorization"])]
    (when jwt-token
      (extract-user-id jwt-token))))

;; string ring-reqponce --> bool
(defn is-lvl [lvl ctx]
  (let [jwt-token (get-in ctx [:request :headers "authorization"])]
    (when jwt-token
      (cond
        (= "read" lvl) (can-user-read jwt-token)
        (= "write" lvl) (can-user-write jwt-token)
        :else false))))
