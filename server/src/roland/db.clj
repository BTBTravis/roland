(ns roland.db
  (:require [monger.core :as mg]
            [monger.operators :refer :all]
            [monger.collection :as mc]
            [java-time :as time])
  (:import [org.bson.types ObjectId]
           [com.mongodb DB WriteConcern]))

;; helper functions
;; str --> fun
(defn parse-interval [str]
  (let [[_ amount unit] (re-matches #"^(\d+)([dwmy])$" str)]
    (when (and amount unit) ;; successful match
      (cond
        (= unit "d") (fn [t] (time/plus t (time/days (Long/valueOf amount))))
        (= unit "w") (fn [t] (time/plus t (time/days (* 7 (Long/valueOf amount)))))
        (= unit "m") (fn [t] (time/plus t (time/days (* 30 (Long/valueOf amount)))))
        (= unit "y") (fn [t] (time/plus t (time/days (* 360 (Long/valueOf amount)))))))))

;; create user
;; user --> bool
(defn validate-user [user]
  (let [rules [(contains? user :username)]]
    (= (into [] (repeat (count rules) true)) rules)))
;; user --> ??
(defn insert-user [user]
  (let [conn (mg/connect)
        db   (mg/get-db conn "roland")
        valid (validate-user user)]
    (when valid (mc/insert-and-return db "users" user))))

;; id --> user
(defn get-user [id]
  (let [conn (mg/connect)
        db   (mg/get-db conn "roland")
        coll "users"]
    (mc/find-one-as-map db "users" {:_id (ObjectId. id)})))

;; user --> str
(defn get-user-id [user] (when user (.toString (get user :_id))))

;; link
;; link --> bool
(defn validate-half-link [link]
  (let [rules [(contains? link :url)
               (contains? link :title)
               (contains? link :interval)]]
    (= (into [] (repeat (count rules) true)) rules)))

;; link --> link
(defn complete-link [link]
  (assoc link :created_at (.toString (time/local-time)) :interval_date (.toString ((parse-interval (:interval link)) (time/local-date)))))

;; link --> bool
(defn validate-link [link]
  (let [rules [(contains? link :url)
               (contains? link :title)
               (contains? link :interval)
               (contains? link :created_at)
               (contains? link :interval_date)]]
    (= (into [] (repeat (count rules) true)) rules)))
;; string --> update object ex: #object[com.mongodb.WriteResult 0x60e392f3 "WriteResult{, n=1, updateOfExisting=true, upsertedId=null}"]
(defn insert-link [id link]
  (let [conn (mg/connect)
        db   (mg/get-db conn "roland")
        coll "users"
        full-link (complete-link link)]
    (when (validate-link full-link)
      (mc/update db "users" {:_id (ObjectId. id)}  {$push {:links full-link}}))))

;; id --> hashmap
(defn get-links [id]
  (let [user (get-user id)]
    (:links user)))

(defn delete-link [id link]
  (let [conn (mg/connect)
        db   (mg/get-db conn "roland")
        coll "users"
        links (filter (fn [x] (not (= (:url x) (:url link)))) (get-links id))]
    (mc/update db "users" {:_id (ObjectId. id)}  {:links links})))

(defn wrap-links [links]
  (map (fn [link] [link (assoc link :interval_date (time/local-date (:interval_date link)))]) links))

(defn unwrap-links [links]
  (map (fn [pair] (get pair 0)) links))

(defn get-today-links [id]
  (unwrap-links (filter (fn [pair] (= (time/local-date) (:interval_date (get pair 1))))
                        (wrap-links (get-links id)))))

(defn get-backlog-links [id]
  (unwrap-links (filter (fn [pair] (time/after? (time/local-date) (:interval_date (get pair 1))))
                        (wrap-links (get-links id)))))
