(defproject roland "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :main rolan.core
  :plugins [[lein-ring "0.12.2"]]
  :ring {:handler roland.core/handler
         :nrepl {:start? true}}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [liberator "0.15.2"]
                 [compojure "1.6.0"]
                 [com.novemberain/monger "3.1.0"]
                 [nrepl "0.4.5"]
                 [org.clojure/tools.logging "0.4.1"]
                 [buddy/buddy-sign "2.2.0"]
                 [clojure.java-time "0.3.2"]
                 ;; [mount "0.1.13"]
                 ;; [conman "0.8.2"]
                 ;; [clj-time "0.14.4"]
                 ;; [org.xerial/sqlite-jdbc "3.23.1"]
                 [ring/ring-json "0.4.0"]
                 [ring/ring-core "1.6.3"]])
