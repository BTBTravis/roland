module Main exposing (Model, Msg(..), init, main, update, view, viewSendInput)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)



-- MAIN


main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { title : String
    , url : String
    , display_interval : String
    }


init : Model
init =
    { title = ""
    , url = ""
    , display_interval = ""
    }


isLinkEligible : Model -> Bool
isLinkEligible model =
   not (String.length model.title > 0 && String.length model.url > 7 && String.length model.display_interval >= 2)


-- UPDATE


type Msg
    = Title String
    | Url String
    | Display_Interval String


update : Msg -> Model -> Model
update msg model =
    case msg of
        Title title ->
            { model | title = title }

        Url url ->
            { model | url = url }

        Display_Interval display_interval ->
            { model | display_interval = display_interval }



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "send-from__wrapper" ]
        [ div [ class "send-form__double-input-wrapper" ]
            [ input [ class "send-form__input", placeholder "url", value model.url, onInput Url ] []
            , input [ class "send-form__input", placeholder "1m", value model.display_interval, onInput Display_Interval ] []
            ]
        , div [ class "send-form__single-input-wrapper" ]
            [ input [ class "send-form__input", placeholder "title", value model.title, onInput Title ] [] ]
        , button [ class "send-form__btn send-from__btn--submit", disabled (isLinkEligible model) ] [ text "send" ]

        -- , button [ class "send-form__btn send-from__btn--submit", onClick Increment ] [ text "+" ]
        -- , viewValidation model
        ]


viewSendInput : String -> String -> (String -> msg) -> Html msg
viewSendInput p v toMsg =
    div [ class "send-form__input-wrapper" ]
        [ input [ class "send-form__input", placeholder p, value v, onInput toMsg ] []
        ]



-- viewValidation : Model -> Html msg
-- viewValidation model =
--   if model.password == model.passwordAgain then
--     div [ style "color" "green" ] [ text "OK" ]
--   else
--     div [ style "color" "red" ] [ text "Passwords do not match!" ]
